;; Copyright (C) 2017 Marc Nieper-Wißkirchen

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;> Graph algorithms.

(define-library (rapid graph)
  (export digraph
	  digraph?
	  digraph->alist
	  alist->digraph
	  digraph-successor-set
	  digraph-predecessor-set
	  digraph-label-set
	  digraph-label-comparator
	  digraph-adjoin-edge
	  digraph-adjoin-node
	  digraph-node-count
	  digraph-find
	  digraph-fold
	  digraph-sccs)
  (import (scheme base)   (scheme write)
	  (rapid receive)
	  (rapid list)
	  (rapid set)
	  (rapid mapping))
  (include "graph.scm"))
