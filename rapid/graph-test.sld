;; Copyright (C) 2017 Marc Nieper-Wißkirchen

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-library (rapid graph-test)
  (export run-tests)
  (import (scheme base)
	  (rapid test)
	  (rapid comparator)
	  (rapid set)
	  (rapid mapping)
	  (rapid graph))
  (begin
    (define comparator (make-default-comparator))
    
    (define (run-tests)
      (test-begin "Graph algorithms")

      (test-group "Constructors"

	(test-assert "digraph?"
	  (digraph? (digraph comparator))))
      
      (test-group "Accessors"
	(define digraph1 (digraph comparator))
      	(define digraph2 (digraph-adjoin-node digraph1 1 2 3 4))
	(define digraph3 (digraph-adjoin-edge digraph2 2 3))
	(define digraph4 (digraph-adjoin-edge digraph3 5 6))
	
	(test-equal "digraph-label-comparator"
	  comparator
	  (digraph-label-comparator digraph1))

	(test-assert "digraph-successor-set: 1"
	  (set=? (set comparator 2 3 4)
		 (digraph-successor-set digraph3 1)))

	(test-assert "digraph-successor-set: 2"
	  (set=? (set comparator 3)
		 (digraph-successor-set digraph3 2)))
	(test-assert "digraph-successor-set: 3"
	  (set=? (set comparator)
		 (digraph-successor-set digraph3 3)))

	(test-assert "digraph-predecessor-set"
	  (set=? (set comparator 1 2)
		 (digraph-predecessor-set digraph3 3)))

	(test-assert "digraph-label-set"
	  (set=? (set comparator 1 2 3 4 5 6)
		 (digraph-label-set digraph4))))

      (test-group "The whole graph"
	(define digraph1 (digraph comparator))
      	(define digraph2 (digraph-adjoin-node digraph1 1 2 3 4))
	(define digraph3 (digraph-adjoin-edge digraph2 2 3))
	(define digraph4 (digraph-adjoin-edge digraph3 5 6))

	(test-equal "digraph-find: found"
	  1
	  (digraph-find (lambda (label successors predecessors)
			  (= (set-size successors) 3))
			digraph3
			(lambda () #f)))

	(test-equal "digraph-find: not found"
	  #f
	  (digraph-find (lambda (label successors predecessors)
			  (= (set-size successors) 4))
			digraph3
			(lambda () #f)))

	(test-equal "digraph-node-count"
	  6
	  (digraph-node-count digraph4)))

      (test-group "Mapping and folding"
	(define digraph1 (digraph comparator))
      	(define digraph2 (digraph-adjoin-node digraph1 1 2 3 4))
	(define digraph3 (digraph-adjoin-edge digraph2 2 3))
	(define digraph4 (digraph-adjoin-edge digraph3 5 6))

	(test-equal "digraph-fold"
	  1
	  (digraph-fold (lambda (label successors predecessors acc)
			  (if (= (set-size successors) 3)
			      (+ 1 acc)
			      acc))
			0 digraph3)))

      (test-group "Copying and conversion"
	(define digraph1 (digraph comparator))
      	(define digraph2 (digraph-adjoin-node digraph1 1 2 3 4))
	(define digraph3 (digraph-adjoin-edge digraph2 2 3))
	(define digraph4 (digraph-adjoin-edge digraph3 5 6))
	(define digraph5 (alist->digraph comparator '((1 2 3) (4 5))))
	
	(test-equal "digraph->alist"
	  4
	  (length (assq 1 (digraph->alist digraph4))))

	(test-equal "alist->digraph"
	  2
	  (set-size (digraph-successor-set digraph5 1))))

      (test-group "Graph algorithms"
	(define digraph1 (alist->digraph comparator
					 '((t f)
					   (f e)
					   (e o)
					   (o e))))
	(define digraph2 (alist->digraph comparator
					  '((t g s)
					    (g s r)
					    (s r f)
					    (r f q)
					    (f q)
					    (q))))

	(define digraph3 (alist->digraph comparator
					 '((t f y)
					   (f y x)
					   (y x)
					   (x y))))	
	(test-equal "digraph-sccs: 1"
	  '(1 1 2)
	  (map length (digraph-sccs digraph1)))

	(test-equal "digraph-sccs: 2"
	  '(1 1 1 1 1 1)
	  (map length (digraph-sccs digraph2)))

	(test-equal "digraph-sccs: 3"
	  '(1 1 2)
	  (map length (digraph-sccs digraph3))))
      
      (test-end))))
