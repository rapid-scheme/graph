;; Copyright (C) 2017 Marc Nieper-Wißkirchen

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;> \section{Directed graphs}

;;> A \emph{directed graph} or \emph{digraph} consists of a set of
;;> \emph{nodes} and a set of \emph{edges}.  Each node is
;;> distinguished by its \emph{label}.  Each edge has a \emph{source}
;;> node and a \emph{target} node.  There is at most one edge between
;;> two nodes.  The nodes that are targets of edges whose source is a
;;> given node are the \emph{successors} of the given node.  The
;;> \emph{predecessors} of a given node are those nodes for which the
;;> given node is a successor.

(define-record-type <digraph>
  (make-digraph nodes)
  digraph?
  (nodes digraph-nodes))

(define-record-type <neighborhood>
  (make-neighborhood successors predecessors)
  node?
  (successors successors)
  (predecessors predecessors))

;;;; Prodecures

;;> \section{Procedures}

(define (neighborhood comparator)
  (make-neighborhood (set comparator) (set comparator)))

(define (neighborhood-adjoin-successor neighborhood . new-successors)
  (make-neighborhood (apply set-adjoin (successors neighborhood) new-successors)
		     (predecessors neighborhood)))

(define (neighborhood-adjoin-predecessor neighborhood . new-predecessors)
  (make-neighborhood (successors neighborhood)
		     (apply set-adjoin (predecessors neighborhood) new-predecessors)))

;;; Constructors

;;> \procedure{(digraph comparator)}

;;> Returns a digraph without any node whose labels are compared using
;;> \var{comparator}.

(define (digraph comparator)
  (make-digraph (mapping comparator)))

;;> \procedure{(digraph? obj)}

;;> Returns \scheme{#t} if \var{obj} is a digraph and \scheme{#f} otherwise.

;;; Accessors

;;> \procedure{(digraph-label-comparator digraph)}

;;> Returns the label comparator of the \var{digraph}.

(define (digraph-label-comparator digraph)
  (mapping-key-comparator (digraph-nodes digraph)))

;;> \procedure{(digraph-label-set digraph)}

;;> Returns the set of all labels of the nodes of the \var{digraph}.

(define (digraph-label-set digraph)
  (mapping-fold (lambda (label neighborhood set)
		  (set-adjoin set label))
		(set (digraph-label-comparator digraph))
		(digraph-nodes digraph)))

;;> \procedure{(digraph-successor-set digraph label)}

;;> Returns the set of labels of all successors of the node labeled
;;> \var{label} in the \var{digraph}.

(define (digraph-successor-set digraph label)
  (successors (mapping-ref (digraph-nodes digraph) label)))

;;> \procedure{(digraph-predecessor-set digraph label)}

;;> Returns the set of labels of all predecessors of the node labeled
;;> \var{label} in the \var{digraph}.

(define (digraph-predecessor-set digraph label)
  (predecessors (mapping-ref (digraph-nodes digraph) label)))

;; Updaters

;;> \procedure{(digraph-adjoin-edge digraph source target)}

;;> Returns a digraph that results from adjoining an edge between the
;;> nodes labeled \var{source} and \var{target} to the \var{digraph}.
;;> If there is already an edge between the \var{source} and
;;> \var{target}, no edge is added.  If there is no node labeled
;;> \var{source} or \var{target}, a node with that label is adjoined
;;> as well.

(define (digraph-adjoin-edge digraph source target)
  (let*
      ((comparator (digraph-label-comparator digraph))
       (neighborhood-maker (lambda ()
			     (neighborhood comparator))) 
       (nodes (digraph-nodes digraph))
       (nodes (mapping-update nodes source
			      (lambda (nbhd)
				(neighborhood-adjoin-successor nbhd target))
			      neighborhood-maker))
       (nodes (mapping-update nodes target
			      (lambda (nbhd)
				(neighborhood-adjoin-predecessor nbhd source))
			      neighborhood-maker)))
    (make-digraph nodes)))

;;> \procedure{(digraph-adjoin-node digraph label . targets)}

;;> Returns a digraph that results from adjoining a node labeled
;;> \var{label} and edges from that node to the nodes labeled
;;> \var{targets} to the \var{digraph}.  If there is already a node
;;> \var{label}, no node is added.  If there is already an edge
;;> between the \var{label} and one of the \var{targets}, that edge is not added.  If
;;> there is no node labeled by one of \var{targets}, a node with
;;> that label is adjoined as well.

(define (digraph-adjoin-node digraph label . targets)
  (let*
      ((comparator (digraph-label-comparator digraph))
       (neighborhood-maker (lambda ()
			     (neighborhood comparator)))      
       (nodes (digraph-nodes digraph))
       (nodes (mapping-update nodes label
			      (lambda (nbhd)
				(apply neighborhood-adjoin-successor nbhd targets))
			      neighborhood-maker))
       (nodes (fold (lambda (target nodes)
                      (mapping-update nodes target
				      (lambda (nbhd)
					(neighborhood-adjoin-predecessor nbhd label))
				      neighborhood-maker))      
                    nodes targets)))
    (make-digraph nodes)))

;; The whole graph

;;> \procedure{(digraph-node-count digraph)}

;;> Returns the number of nodes of the \emph{digraph}.

(define (digraph-node-count digraph)
  (mapping-size (digraph-nodes digraph)))

;;> \procedure{(digraph-find predicate digraph failure)}

;;> Returns the label of an arbitrarily chosen node of the
;;> \var{digraph} such that \var{predicate} returns a true value when
;;> invoked with the label and the nodes successor and predecessor
;;> sets as arguments, or the result of tail-calling \var{failure}
;;> with no arguments when there is none.

(define (digraph-find predicate digraph failure)
  (receive (label neighborhood)
      (mapping-find (lambda (label nbhd)
		      (predicate label
				 (successors nbhd)
				 (predecessors nbhd)))
		    (digraph-nodes digraph)
		    failure)
    label))

;; Mapping and folding

;;> \procedure{(digraph-fold proc nil digraph)}

;;> Invokes \var{proc} for each node of the \var{digraph} with three
;;> arguments: the label of the node, its successor set, its
;;> predecessor set, and an accumulated result of the previous
;;> invokation.  For the first invokation, \var{nil} is used as the
;;> last argument.  Returns the result of the last invokation or
;;> \var{nil} if there was no invokation.

(define (digraph-fold proc nil digraph)
  (mapping-fold (lambda (label nbhd result)
		  (proc label
			(successors nbhd)
			(predecessors nbhd)
			result))
		nil (digraph-nodes digraph)))

;; Copying and conversion

;;> \procedure{(digraph->alist digraph)}

;;> Returns a newly allocated association list with one association
;;> for each node of \var{digraph}.  Each association is a pair whose
;;> car is the label and whose cdr is a newly allocated list of the
;;> successors' labels of the node.

(define (digraph->alist digraph)
  (digraph-fold (lambda (label successors predecessors alist)
                  (cons (cons label (set->list successors))
                        alist))
                '()
                digraph))

;;> \procedure({alist->digraph comparator alist})

;;> Returns a newly allocated digraph, created as if by
;;> \scheme{digraph} using the comparator \var{comparator}, that has
;;> one node per association in \var{alist}.  Each association is a
;;> pair whose car is the label and whose cdr is a list of the
;;> successors' labels of the node.

(define (alist->digraph comparator alist)
  (fold (lambda (labels digraph)
          (apply digraph-adjoin-node digraph labels))
        (digraph comparator)
        alist))

;;; Algorithms

;;> \procedure{(digraph-sccs digraph)}

;;> Returns a topologically sorted list of the strongly-connected
;;> components of the \var{digraph}.  Each strongly-connected
;;> component is a newly allocated list of the labels of the nodes
;;> belonging to that strongly-connected component.

(define (digraph-sccs digraph)
  (let* ((node-count (digraph-node-count digraph))
	 (comparator (digraph-label-comparator digraph))
	 (indices (mapping comparator))
	 (labels (make-vector node-count))
	 (successor-lists (make-vector node-count))
	 (lowlinks (make-vector node-count))
	 (stacked (make-vector node-count #f))
	 (index 0)
	 (stack '()))
    (digraph-fold
     (lambda (label successors predecessors sccs)
       (if (mapping-ref/default indices label #f)
	   sccs
	   (receive (w sccs)
	       (let loop ((label label)
			  (successors successors)
			  (sccs sccs))
		 (let ((v index))
		   (set! indices (mapping-set indices label index))
		   (vector-set! labels v label)
		   (vector-set! lowlinks v index)
		   (set! index (+ index 1))
		   (set! stack (cons v stack))
		   (vector-set! stacked v #t)
		   (receive (sccs)
		       (set-fold
			(lambda (successor sccs)
			  (cond
			   ((mapping-ref/default indices successor #f)
			    => (lambda (w)
				 (when (vector-ref stacked w)
				   (vector-set! lowlinks
						v
						(min (vector-ref lowlinks v)
						     w)))
				 sccs))
			   (else
			    (receive (w sccs)
				(loop successor
				      (digraph-successor-set digraph successor)
				      sccs)
			      (vector-set! lowlinks v
					   (min (vector-ref lowlinks v)
						(vector-ref lowlinks w)))
			      sccs))))
			sccs successors)
		     (if (= (vector-ref lowlinks v) v)
			 (let loop ((scc '()))
			   (let* ((w (car stack))
				  (scc (cons (vector-ref labels w) scc)))
			     (set! stack (cdr stack))
			     (vector-set! stacked w #f)
			     (if (= v w)
				 (values v (cons scc sccs))
				 (loop scc))))
			 (values v sccs)))))
	     sccs)))
     '() digraph)))
